require 'byebug'
class RPNCalculator

  def initialize
    @numbers = []
  end

  def push(num)
    @numbers << num
  end

  def value
    @numbers.last
  end

  def plus
    if @numbers.length <= 1
      raise "calculator is empty"
    end
    val = @numbers.pop + @numbers.pop
    @numbers.push(val)
  end

  def minus
    if @numbers.length <= 1
      raise "calculator is empty"
    end
    subtractor = @numbers.pop
    val = @numbers.pop - subtractor
    @numbers.push(val)
  end

  def divide
    if @numbers.length <= 1
      raise "calculator is empty"
    end
    divisor = @numbers.pop
    val = @numbers.pop / divisor.to_f
    @numbers.push(val)
  end

  def times
    if @numbers.length <= 1
      raise "calculator is empty"
    end
    multiplier = @numbers.pop
    val = @numbers.pop * multiplier.to_f
    @numbers.push(val)
  end

  def tokens(str)
    operation = []
    nums = '1234567890'
    str.split(' ').each do |num|
      if nums.include?(num)
        operation << num.to_i
      else
        operation << num.to_sym
      end
    end
    operation
  end

require 'byebug'
  def evaluate(str)
    integers = '1234567890'
    calc = tokens(str)
    num = 0
    i = 0
    while i < calc.length
      # debugger
      if !integers.include?(calc[i].to_s)
        if calc[i] == :+
          num = calc[i - 2]
          num += calc[i - 1]
          calc.delete_at(i)
          calc.delete_at(i - 1)
          calc.delete_at(i - 2)
          calc.insert(i-2, num)
          i -= 2
        elsif calc[i] == :-
          num = calc[i - 2]
          num -= calc[i - 1]
          calc.delete_at(i)
          calc.delete_at(i - 1)
          calc.delete_at(i - 2)
          calc.insert(i-2, num)
          i -= 2
        elsif calc[i] == :*
          num = calc[i - 2]
          num *= calc[i - 1]
          calc.delete_at(i)
          calc.delete_at(i - 1)
          calc.delete_at(i - 2)
          calc.insert(i-2, num)
          i -= 2
        elsif calc[i] == :/
          num = calc[i - 2].to_f
          num /= calc[i - 1]
          calc.delete_at(i)
          calc.delete_at(i - 1)
          calc.delete_at(i - 2)
          calc.insert(i-2, num)
          i -= 2
        end
      end
      i += 1
    end
    num
  end

end
